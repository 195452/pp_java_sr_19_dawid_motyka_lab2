/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.project;

/**
 *
 * @author Dawid
 */
public class Karta {
    private int figura;
    private int kolor;
    private int wartosc;
    private final String[] figuryArray = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    private final String[] koloryArray = {"\u2660","\u2665","\u2666","\u2663"}; //pik, kier, karo, trefl
    public Karta(int figura_, int kolor_)
    {
        figura=figura_;
        kolor=kolor_;
        if (figura<11)
            wartosc=figura;
        else
            if (figura<14)
                wartosc=10;
            else wartosc=11;
    }
    public String getFigura()
    {
        return figuryArray[figura-2];
    }
    public int getWartosc()
    {
        return wartosc;
    }
    @Override
    public String toString()
    {
        return  koloryArray[kolor] + getFigura();
    }
    
}
