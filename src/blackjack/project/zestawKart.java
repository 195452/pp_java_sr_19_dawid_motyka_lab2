/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.project;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Dawid
 */
public class zestawKart {

    ArrayList<Karta> karty = new ArrayList<>();

    public int sumaKart() {
        int suma = 0;
        for (Karta karta : karty) {
            suma += karta.getWartosc();
        }
        return suma;
    }

    void dodajKarte(Karta karta)
    {
        karty.add(karta);
    }
    
    public String wypiszKarty(int tryb) //0 - gracz, 1 - krupier
    {
        String kartyString = "";
        Iterator itr = karty.iterator();
        if (itr.hasNext()) {
            if (tryb == 0) {
                kartyString += itr.next() + " ";
            } else {
                itr.next();
                kartyString += "XX ";
            }
            while (itr.hasNext()) {
                kartyString += itr.next() + " ";
            }
        } else {
            return "";
        }
        return kartyString;
    }

}
