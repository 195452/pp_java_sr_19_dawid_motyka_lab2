/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.project;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Dawid
 */
public class Talia {
    LinkedList<Karta> talia = new LinkedList<>();
    
    int randomRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }
    
    public Talia()
    {
        for(int i=0;i<4;i++)
            for(int j=2;j<15;j++)
            {
                Karta karta = new Karta(j,i);
                talia.add(karta);
            }    
    }
    
    public Karta wylosujKarte() //losuje i wyciaga z talii
    {
        int wylosowana = randomRange(0, talia.size()-1);
        
        Karta wylosowanaKarta;
        if (wylosowana==0)
        {
            wylosowanaKarta = talia.getFirst();
            talia.removeFirst();
        }
        else  
        {
            ListIterator<Karta> iterator = talia.listIterator(wylosowana);
            wylosowanaKarta = iterator.next();
            iterator.remove();
        }
        
        return wylosowanaKarta;
    }
    
}
