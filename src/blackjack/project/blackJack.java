package blackjack.project;

import java.io.IOException;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dawid
 */
public class blackJack {
    int randomRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }
    int[] losuj_karty(int n)
    {
       int[] karty = new int[n];
       for(int i=0; i<n; i++)
       {
           karty[i]=randomRange(2, 11);
       }
       return karty;
    }
    void test()
    {

        
    }
    int suma_kart(int[] karty)
    {
        int suma = 0;
        for (int karta: karty)
        {
            suma+=karta;
        }
        return suma;
    }
    void start()
    {
        int suma_gracza=0;
        int suma_krupiera=0;

        boolean koniec=false;
        int [] karty_krupiera=losuj_karty(2);
        int [] karty_gracza=losuj_karty(2);
        suma_gracza = suma_kart(karty_gracza);
        suma_krupiera = suma_kart(karty_krupiera);
        System.out.println("Karty gracza\n"+ Arrays.toString(karty_gracza)+"\nKarty krupiera\n[x, "+ karty_krupiera[1] +"]");
        while(koniec==false)
        {
            boolean krupier_losowal = false;
            boolean gracz_losowal = false;
            System.out.println("Czy dobierasz karte? (wpisz t lub n)");
            int wybor=0;
            while(wybor!=116 && wybor!=110)
            {
                try {
                    wybor = System.in.read();
                    while (System.in.available()>0)
                        System.in.read();
                } catch (IOException e) {
                    System.out.println("Błąd pobierania odpowiedzi");
                }
            }
            if (wybor==116)
            {
                gracz_losowal=true;
                int dobrana = losuj_karty(1)[0];
                suma_gracza+=dobrana;
                System.out.println("Dobrałeś " + dobrana + " suma twoich kart: " + suma_gracza);
            }
            if (suma_krupiera<16)
            {
                krupier_losowal=true;
                int dobrana = losuj_karty(1)[0];
                suma_krupiera+=dobrana;
                System.out.println("Krupier dobrał " + dobrana + " suma odkrytych kart krupiera: " + (suma_krupiera-karty_krupiera[0]));
            }
            else 
                System.out.println("Krupier nie dobiera");
            
            if(suma_gracza>21)
            {
                System.out.println("Przekroczyłeś 21, przegrałeś.");
                koniec = true;
            }
            if (suma_krupiera > 21)
            {
                System.out.println("Krupier przekroczył 21, wygrałeś.");
                koniec = true;
            }
            
            if(!gracz_losowal && !krupier_losowal)
            {
                if (suma_gracza > suma_krupiera)
                    System.out.println("Wygrales, masz większa sumę kart");
                else
                    System.out.println("Przegrales, masz mniejszą sumę kart");
                koniec = true;
            }
            
        }
            
    }
}